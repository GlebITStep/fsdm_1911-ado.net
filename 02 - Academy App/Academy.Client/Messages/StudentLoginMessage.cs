﻿using System;
using System.Collections.Generic;
using System.Text;
using Academy.Shared.Models;

namespace Academy.Client.Messages
{
    class StudentLoginMessage
    {
        public Student Student { get; set; }
    }
}
