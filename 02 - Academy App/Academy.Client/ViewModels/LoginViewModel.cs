﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using Academy.Client.Messages;
using Academy.Shared.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Academy.Client.ViewModels
{
    class LoginViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IStudentRepository _studentRepository;
        private readonly IMessenger _messenger;

        public string Username { get; set; }
        public string Password { get; set; }

        public RelayCommand LoginCommand { get; set; }
        public RelayCommand RegisterCommand { get; set; }

        public LoginViewModel(
            INavigationService navigationService,
            IStudentRepository studentRepository,
            IMessenger messenger)
        {
            _navigationService = navigationService;
            _studentRepository = studentRepository;
            _messenger = messenger;

            LoginCommand = new RelayCommand(Login);
            RegisterCommand = new RelayCommand(Register);
        }

        private void Login()
        {
            try
            {
                var student = _studentRepository.GetByUsername(Username);
                var saltedPassword = Password + student.Salt;
                var passwordBytes = Encoding.UTF8.GetBytes(saltedPassword);
                var algorithm = new SHA256Managed();
                var hash = algorithm.ComputeHash(passwordBytes);
                var hasedPassword = Convert.ToBase64String(hash);

                if (hasedPassword != student.PasswordHash)
                {
                    MessageBox.Show("Error!");
                    return;
                }

                _messenger.Send(new StudentLoginMessage { Student = student });
                _navigationService.NavigateTo<HomeViewModel>();

            }
            catch (Exception)
            {
                MessageBox.Show("Username not found!");
            }
        }

        private void Register()
        {
            _navigationService.NavigateTo<RegisterViewModel>();
        }
    }
}
