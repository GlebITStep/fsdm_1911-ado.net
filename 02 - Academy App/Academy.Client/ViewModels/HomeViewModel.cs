﻿using System;
using System.Collections.Generic;
using System.Text;
using Academy.Client.Messages;
using Academy.Shared.Models;
using Academy.Shared.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Academy.Client.ViewModels
{
    class HomeViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IMessenger _messenger;

        public Student Student { get; set; }

        public RelayCommand LogoutCommand { get; set; }

        public HomeViewModel(
            INavigationService navigationService,
            IMessenger messenger)
        {
            _navigationService = navigationService;
            _messenger = messenger;

            LogoutCommand = new RelayCommand(Logout);

            messenger.Register<StudentLoginMessage>(this, message => Student = message.Student);
        }

        private void Logout()
        {
            _navigationService.NavigateTo<LoginViewModel>();
        }
    }
}
