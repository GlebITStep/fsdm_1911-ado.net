﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Academy.Shared.Messages
{
    public class NavigationMessage
    {
        public Type ViewModelType { get; set; }
    }
}
