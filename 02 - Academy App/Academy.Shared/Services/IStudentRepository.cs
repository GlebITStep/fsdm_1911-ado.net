﻿using System.Collections.Generic;
using Academy.Shared.Models;

namespace Academy.Shared.Services
{
    public interface IStudentRepository
    {
        IEnumerable<Student> Get();
        Student GetById(int id);
        Student GetByUsername(string username);
        int Create(Student student);
        bool Update(Student student);
        void Delete(int id);
        void UpdatePassword(int id, string passwordHash, string salt);
    }
}
