﻿using System;
using System.Collections.Generic;
using System.Data;
using Academy.Shared.Models;
using Microsoft.Data.SqlClient;

namespace Academy.Shared.Services
{
    public class StudentRepository : IStudentRepository
    {
        private readonly string ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Academy";

        public IEnumerable<Student> Get()
        {
            var query = "SELECT Id, FirstName, LastName, Coins, Username, PasswordHash, Salt, LastEdit FROM Student";
            var students = new List<Student>();

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand(query, connection);
                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var student = new Student
                        {
                            Id = reader.GetInt32(0),
                            FirstName = reader.GetString(1),
                            LastName = reader.GetString(2),
                            Coins = reader.GetInt32(3),
                            Username = reader.GetString(4),
                            PasswordHash = reader.GetValue(5) as string,
                            Salt = reader.GetValue(6) as string,
                            LastEdit = reader.GetDateTime(7)
                        };
                        students.Add(student);
                    }
                }
            }

            return students;
        }

        public Student GetById(int id)
        {
            var query = "SELECT Id, FirstName, LastName, Coins, Username, PasswordHash, Salt, LastEdit FROM Student WHERE Id = @id";
            Student student = null;

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("@id", id));
                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        student = new Student
                        {
                            Id = reader.GetInt32(0),
                            FirstName = reader.GetString(1),
                            LastName = reader.GetString(2),
                            Coins = reader.GetInt32(3),
                            Username = reader.GetString(4),
                            PasswordHash = reader.GetValue(5) as string,
                            Salt = reader.GetValue(6) as string,
                            LastEdit = reader.GetDateTime(7)
                        };
                    }
                }
                else
                    throw new Exception("Student not found!");
            }

            return student;
        }

        public Student GetByUsername(string username)
        {
            var query = "SELECT Id, FirstName, LastName, Coins, Username, PasswordHash, Salt, LastEdit FROM Student WHERE Username = @username";
            Student student = null;

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("@username", username));
                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        student = new Student
                        {
                            Id = reader.GetInt32(0),
                            FirstName = reader.GetString(1),
                            LastName = reader.GetString(2),
                            Coins = reader.GetInt32(3),
                            Username = reader.GetString(4),
                            PasswordHash = reader.GetValue(5) as string,
                            Salt = reader.GetValue(6) as string,
                            LastEdit = reader.GetDateTime(7)
                        };
                    }
                }
                else
                    throw new Exception("Student not found!");
            }

            return student;
        }

        public int Create(Student student)
        {
            var query = @"INSERT INTO Student (FirstName, LastName, Username) 
                          VALUES (@firstName, @lastName, @username); 
                          SET @id=SCOPE_IDENTITY();";
            var id = 0;

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("@firstName", student.FirstName));
                command.Parameters.Add(new SqlParameter("@lastName", student.LastName));
                command.Parameters.Add(new SqlParameter("@username", student.Username));

                var idParam = new SqlParameter { ParameterName = "@id", DbType = DbType.Int32, Direction = ParameterDirection.Output};
                command.Parameters.Add(idParam);

                command.ExecuteNonQuery();
                id = (int)idParam.Value;
            }

            return id;
        }

        public bool Update(Student student)
        {
            var query = @"  UPDATE Student
                            SET FirstName = @firstName, LastName = @lastName, Coins = @coins, LastEdit = CURRENT_TIMESTAMP
                            WHERE Id = @id AND LastEdit = @lastEdit";

            int result = 0;

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("@firstName", student.FirstName));
                command.Parameters.Add(new SqlParameter("@lastName", student.LastName));
                command.Parameters.Add(new SqlParameter("@coins", student.Coins));
                command.Parameters.Add(new SqlParameter("@id", student.Id));
                command.Parameters.Add(new SqlParameter("@lastEdit", student.LastEdit));
                result = command.ExecuteNonQuery();
            }

            return result == 1;
        }

        public void Delete(int id)
        {
            var query = "DELETE FROM Student WHERE Id = @id";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("@id", id));
                command.ExecuteNonQuery();
            }
        }

        public void UpdatePassword(int id, string passwordHash, string salt)
        {
            var query = @"  UPDATE Student
                            SET PasswordHash = @passwordHash, Salt = @salt
                            WHERE Id = @id";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("@passwordHash", passwordHash));
                command.Parameters.Add(new SqlParameter("@salt", salt));
                command.Parameters.Add(new SqlParameter("@id", id));
                command.ExecuteNonQuery();
            }
        }
    }
}