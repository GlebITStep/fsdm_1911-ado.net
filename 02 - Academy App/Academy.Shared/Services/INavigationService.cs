﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight;

namespace Academy.Shared.Services
{
    public interface INavigationService
    {
        void NavigateTo<T>() where T : ViewModelBase;
    }
}
