﻿CREATE DATABASE Academy
USE Academy
GO

CREATE TABLE Student (
	Id INT PRIMARY KEY IDENTITY,
	FirstName NVARCHAR(100) NOT NULL,
	LastName NVARCHAR(100) NOT NULL,
	Coins INT NOT NULL DEFAULT(0),
	Username VARCHAR(100) NOT NULL,
	PasswordHash VARCHAR(100) NULL,
	Salt VARCHAR(100) NULL,

	CONSTRAINT CK_Student_Coins CHECK(Coins >= 0),
	CONSTRAINT UQ_Student_Username UNIQUE(Username)
);

CREATE TABLE Product (
	Id INT PRIMARY KEY IDENTITY,
	Title NVARCHAR(100) NOT NULL,
	Price INT NOT NULL,
	Quantity INT NOT NULL DEFAULT(0),

	CONSTRAINT CK_Product_Quantity CHECK(Quantity >= 0)
)

ALTER TABLE Student
ALTER COLUMN LastEdit DATETIME NOT NULL

ALTER TABLE Student
ADD CONSTRAINT DF_Student_LastEdit DEFAULT CURRENT_TIMESTAMP FOR LastEdit;

--TEST--

INSERT INTO Student (FirstName, LastName, Username) 
VALUES (N'Gleb', N'Skripnikov', 'GlebS')

SELECT * FROM Student

SELECT Id, FirstName, LastName, Coins, Username, PasswordHash, Salt FROM Student WHERE Id = 1

DELETE FROM Student
WHERE Id = 1

UPDATE Student
SET FirstName = 'Farruh', LastName = 'Test', Coins = 10, LastEdit = CURRENT_TIMESTAMP
WHERE Id = 3 AND LastEdit = '2020-06-25 13:02:42'


SELECT CURRENT_TIMESTAMP