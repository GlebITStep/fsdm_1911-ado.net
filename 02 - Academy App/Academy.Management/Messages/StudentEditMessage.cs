﻿using System;
using System.Collections.Generic;
using System.Text;
using Academy.Shared.Models;

namespace Academy.Management.Messages
{
    class StudentEditMessage
    {
        public Student Student { get; set; }
    }
}
