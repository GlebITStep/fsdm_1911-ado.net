﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using Academy.Management.Messages;
using Academy.Shared.Extensions;
using Academy.Shared.Models;
using Academy.Shared.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Academy.Management.ViewModels
{
    class StudentsViewModel : ViewModelBase
    {
        private readonly IStudentRepository _studentRepository;
        private readonly INavigationService _navigationService;
        private readonly IMessenger _messenger;

        public ObservableCollection<Student> Students { get; set; } = new ObservableCollection<Student>();

        public RelayCommand AddCommand { get; set; }
        public RelayCommand<Student> EditCommand { get; set; }
        public RelayCommand<Student> DeleteCommand { get; set; }

        public StudentsViewModel(
            IStudentRepository studentRepository, 
            INavigationService navigationService,
            IMessenger messenger)
        {
            _studentRepository = studentRepository;
            _navigationService = navigationService;
            _messenger = messenger;

            LoadStudents();

            AddCommand = new RelayCommand(AddStudent);
            EditCommand = new RelayCommand<Student>(EditStudent);
            DeleteCommand = new RelayCommand<Student>(DeleteStudent);

            _messenger.Register<StudentChangedMessage>(this, message => LoadStudents());
        }

        private void LoadStudents()
        {
            Students = _studentRepository.Get().ToObservableCollection();
        }

        private void AddStudent()
        {
            _navigationService.NavigateTo<StudentEditorViewModel>();
        }

        private void EditStudent(Student student)
        {
            _messenger.Send(new StudentEditMessage { Student = student });
            _navigationService.NavigateTo<StudentEditorViewModel>();
        }

        private void DeleteStudent(Student student)
        {
            _studentRepository.Delete(student.Id);
            LoadStudents();
        }
    }
}
