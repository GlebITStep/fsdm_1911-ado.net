﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using Academy.Management.Messages;
using Academy.Shared.Models;
using Academy.Shared.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Academy.Management.ViewModels
{
    class StudentEditorViewModel : ViewModelBase
    {

        private readonly IStudentRepository _studentRepository;
        private readonly INavigationService _navigationService;
        private readonly IMessenger _messenger;
        private Student _studentToEdit;

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Coins { get; set; }
        public bool IsEdit { get; set; } = false;
        public bool IsAdd
        {
            get => !IsEdit;
            set => IsEdit = !value;
        }

        public RelayCommand AddCommand { get; set; }
        public RelayCommand EditCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }

        public StudentEditorViewModel(
            IStudentRepository studentRepository,
            INavigationService navigationService,
            IMessenger messenger)
        {
            _studentRepository = studentRepository;
            _navigationService = navigationService;
            _messenger = messenger;

            AddCommand = new RelayCommand(AddStudent);
            EditCommand = new RelayCommand(EditStudent);
            CancelCommand = new RelayCommand(Cancel);

            _messenger.Register<StudentEditMessage>(this, message =>
            {
                _studentToEdit = message.Student;
                FirstName = _studentToEdit.FirstName;
                LastName = _studentToEdit.LastName;
                Coins = _studentToEdit.Coins;
                IsEdit = true;
            });
        }

        private void AddStudent()
        {
            var student = new Student
            {
                FirstName = FirstName,
                LastName = LastName,
                Username = GenerateUsername(FirstName, LastName)
            };
            student.Id = _studentRepository.Create(student);
            _messenger.Send(new StudentChangedMessage {Id = student.Id});
            _navigationService.NavigateTo<StudentsViewModel>();
            Clear();
        }

        private void EditStudent()
        {
            _studentToEdit.FirstName = FirstName;
            _studentToEdit.LastName = LastName;
            _studentToEdit.Coins = Coins;
            var success = _studentRepository.Update(_studentToEdit);

            if (!success)
            {
                MessageBox.Show("Student was already edited!");
                _studentToEdit = _studentRepository.GetById(_studentToEdit.Id);
                FirstName = _studentToEdit.FirstName;
                LastName = _studentToEdit.LastName;
                Coins = _studentToEdit.Coins;
                return;
            }

            _messenger.Send(new StudentChangedMessage { Id = _studentToEdit.Id });
            _navigationService.NavigateTo<StudentsViewModel>();
            Clear();
        }

        private void Cancel()
        {
            _navigationService.NavigateTo<StudentsViewModel>();
            Clear();
        }

        private void Clear()
        {
            FirstName = string.Empty;
            LastName = string.Empty;
            Coins = 0;
            IsEdit = false;
        }

        private string GenerateUsername(string name, string surname)
        {
            Random rand = new Random();
            var randNumber = rand.Next(1000, 9999);
            return $"{name}{surname.ToUpper()[0]}_{randNumber}";
        }
    }
}
